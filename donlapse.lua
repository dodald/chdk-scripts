--[[
Author: Don Riesbeck

Intervalometer with auto-focus protection

 - Detects when auto-focus fails, if focus fails it uses the previous
   focus. (The script will not run if the first focus attempt fails)
 - The script will adjust the focus slowly over 20 images. (can be changed)
 
]]
--[[
@title donslapser
@param a Delay 1st Shot (Mins)
@default a 0
@param b Delay 1st Shot (Secs)
@default b 5
@param c Number of Shots
@default c 3600
@param d Interval (Minutes)
@default d 0
@param e Interval (Seconds)
@default e 10
@param f Interval (10th Seconds)
@default f 0
@param g Endless?
@default g 0
@param h Focus: 0=1st, 1=Every, 2=every other
@default h 0
@param i FMode? 0=No,1=M,2=SM,3=Inf  
@default i 0
--]]

-- make readable variables (also some simple calculations.)
delay_start=a*60000+b*1000
if (delay_start < 0) then
    delay_start=0
end
shot_interval=d*60000+e*1000+f*100
sched_shots=c
if g > 0 then
    sched_shots=0
end

focus_interval=h
focus_mode=i
if focus_mode==3 then
 -- focus_interval=1
end
focus_duration=20

-- the clock time that the first image should be captured.
start_ticks = get_tick_count() + delay_start

-- How long before the frame is due should the camera start focusing.
-- The frame will be delayed IF focusing takes longer than this.
-- If the interval is less than the time it takes to focus and shoot
-- the just continually focus and shoot.
focus_allotment = 3500

-- state vars
-- the number of shots that have been taken
shot_count=0
last_focus_value=0
last_focus_used=0

-- Function definitions

function SetupCamera()
    if (focus_mode==1) then
        set_prop(6, 1)
    elseif (focus_mode==2) then
        set_prop(6, 5)
    elseif (focus_mode==3) then
        set_prop(6, 3)
    end    
end

function DoFocus()
    local focus_success=false
    local focus_changed=false
    local current_focus
    local focus_change
    
    if (focus_interval==0) or (shot_count==0) then
        -- Only focus on first shot.
        if (shot_count==0) then
            print ("Setting Initial Focus...")
            if (TryNewFocus()) then
                last_focus_value = CurrentFocusValue()
                last_focus_used = last_focus_value
                focus_success=true
            end
        else
            -- No need to focus.
            focus_success=true
        end
    else
        if ((shot_count % focus_interval) == 0) then
            print ("Refocusing...")
            if (TryNewFocus()) then
                focus_changed=true
                last_focus_value = CurrentFocusValue()
            end
        end
        CalculateNextFocusValue()
        current_focus = CurrentFocusValue()
        if (IsFocused() == false) or not (current_focus == last_focus_used) then
            focus_changed=true
            focus_change = last_focus_used - current_focus;
            print ("Focus:CUR=" .. last_focus_used .. "(" .. focus_change .."),SD=" .. last_focus_value)
            ForceFocus(last_focus_used)
        end
        focus_success = true
    end
    return focus_success
end

function CalculateNextFocusValue()
    local newvalue = last_focus_used
    local diff = math.abs(last_focus_used - last_focus_value);
    local change = ((diff * (1000 +(1000/focus_interval))) / 1000) - diff
    if (change < 1) then
        change = 1
    end

    if (newvalue > last_focus_value) then
        newvalue = newvalue - change
    elseif (newvalue < last_focus_value) then
        newvalue = newvalue + change
    end
    last_focus_used = newvalue
end

function IsFocused()
    return get_prop(18) > 0
end

function CurrentFocusValue()
    return get_focus()
end

function TryNewFocus()
    local focus_complete = false
    local attempts = 0

    repeat
        attempts = attempts + 1
        set_aflock(1)
        if (IsFocused() or (focus_mode==3)) then
            focus_complete = true
	    
        end
	release("shoot_half")
    until focus_complete or attempts > 2

    return focus_complete
end

function ForceFocus(subject_dist)
    --set_aflock(0)
    set_focus(subject_dist)
    --set_aflock(1)
end

function SleepTilTick(dueTime)
    local sleep_time = dueTime - get_tick_count();
    if sleep_time > 0 then
        sleep(sleep_time)
    end
end

function SleepForNextFocus()
    local next_focus_due_at = (start_ticks + (shot_count * shot_interval)) - focus_allotment
    
    SleepTilTick(next_focus_due_at)
end

function SleepAndWakeup()
    local next_wakeup_due_at = (start_ticks + (shot_count * shot_interval)) - focus_allotment - 2000
    
    if (next_wakeup_due_at > 0) then
        SleepTilTick(next_wakeup_due_at)
        click("set")
    end
end

function SleepForNextShoot()
    local next_shoot_due_at = (start_ticks + (shot_count * shot_interval))
    
    SleepTilTick(next_shoot_due_at)
end

function DoShoot()
    shoot()
    shot_count = shot_count + 1
    if (sched_shots <= 0) then
        print ("Shot " .. shot_count .. " of " .. get_jpg_count())
    else
        print ("Shot " .. shot_count .. " of " .. sched_shots)
    end
end

--  MAIN 

SetupCamera()



while (sched_shots <= 0) or (sched_shots > shot_count) do
    SleepAndWakeup()
    SleepForNextFocus()
    if (DoFocus()) then
        SleepForNextShoot()
        DoShoot()
    else
        -- Focus can only fail for first shot
        print ("Focus Failure (Exiting)")
        break
    end
end

-- restore
set_aflock(0)